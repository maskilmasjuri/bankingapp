package com.dxc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AdminModel {
	private String username;
	private String password;
	
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet res = null;
	
	public AdminModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded succesfully");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc", "root", "root");
			System.out.println("Connection has been established successfully");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int adminLoginCheck() {
		try {			
			String s = "select * from admin where admin_username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1,  username);
			res = pstmt.executeQuery();
			
			if(res.next()) {
				if (password.equals(res.getString(2))) {	// == for strings compares reference, use .equals()
					System.out.println(1);
					return 1;
				}
				else {
					System.out.println(-1);
					return -1; //invalid admin_password
				}
			}
			else {
				System.out.println(0);
				return 0; //admin_username doesn't exist
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
