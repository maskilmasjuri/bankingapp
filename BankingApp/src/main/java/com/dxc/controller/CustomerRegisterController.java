package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class CustomerRegisterController
 */
public class CustomerRegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String customerName = request.getParameter("customerName");
		String customerUsername = request.getParameter("customerUsername");
		String customerPassword = request.getParameter("customerPassword");
		String customerEmail = request.getParameter("customerEmail");
		String securityQn = request.getParameter("securityQn");
		String securityAns = request.getParameter("securityAns");
		
		CustomerModel customer = new CustomerModel();
		customer.setCustomerName(customerName);
		customer.setCustomerUsername(customerUsername);
		customer.setCustomerPassword(customerPassword);
		customer.setCustomerEmail(customerEmail);
		customer.setBalance(1000);
		customer.setSecurityQn(securityQn);
		customer.setSecurityAns(securityAns);
		
		int entry = customer.registerCustomer();
		
		HttpSession session = request.getSession(true);
		session.setAttribute("customerName", customer.getCustomerName());

		if(entry==1) {
			response.sendRedirect("/BankingApp/customerRegisterSuccess.jsp");
		}
		else if(entry == 0) {
			response.sendRedirect("/BankingApp/customerExistingUsername.html");
		}
	}

}
