package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class CustomerForgotController
 */
public class CustomerForgotController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String customerUsername = request.getParameter("customerUsername");
		
		CustomerModel customer = new CustomerModel();
		customer.setCustomerUsername(customerUsername);
		int entry = customer.forgotPassword();
		
		HttpSession session = request.getSession(true);
		session.setAttribute("customerUsername", customer.getCustomerUsername());
		
		if (entry==1) {
			response.sendRedirect("/BankingApp/SecurityQnController");
		}
		else {
			response.sendRedirect("/BankingApp/customerForgotPasswordNoUsername.html");
		}
	}
}
