package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class SecurityAnsController
 */
public class SecurityAnsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String customerUsername = (String)session.getAttribute("customerUsername");
		String securityAns = request.getParameter("securityAns");
		
		CustomerModel customer = new CustomerModel();
		customer.setCustomerUsername(customerUsername);
		customer.setSecurityAns(securityAns);
		System.out.println("SecurityAnsController securityAns: "+securityAns);
		int entry = customer.checkSecurityAns();
		
		if (entry==1) {
			String securityQn = customer.getSecurityQn();
			session.setAttribute("securityQn", securityQn);
			response.sendRedirect("/BankingApp/customerResetPassword.html");
		}
		else {
			response.sendRedirect("/BankingApp/customerWrongAns.jsp");
		}
	}

}
