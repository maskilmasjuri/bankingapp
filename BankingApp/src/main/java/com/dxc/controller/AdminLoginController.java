package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dxc.model.AdminModel;

/**
 * Servlet implementation class AdminLoginController
 */
public class AdminLoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		AdminModel admin = new AdminModel();
		admin.setUsername(username);
		admin.setPassword(password);
		
		int loginPass = admin.adminLoginCheck();
		
		if(loginPass==1) {
			response.sendRedirect("/BankingApp/adminLoginSuccess.jsp");
		}
		else if(loginPass==0){
			response.sendRedirect("/BankingApp/invalidAdminUsername.html");
		}
		else {
			response.sendRedirect("/BankingApp/invalidAdminPassword.html");
		}
	}

}
