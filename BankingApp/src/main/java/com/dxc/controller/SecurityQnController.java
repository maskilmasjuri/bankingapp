package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class SecurityQnController
 */
public class SecurityQnController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String customerUsername = (String)session.getAttribute("customerUsername");
		
		CustomerModel customer = new CustomerModel();
		customer.setCustomerUsername(customerUsername);
		int entry = customer.promptSecurityQn();
		
		if (entry==1) {
			String securityQn = customer.getSecurityQn();
			session.setAttribute("securityQn", securityQn);
			response.sendRedirect("/BankingApp/securityQnPrompt.jsp");
		}
		else {
			response.sendRedirect("/BankingApp/customerForgotPasswordNoUsername.html");
		}
	}

}
