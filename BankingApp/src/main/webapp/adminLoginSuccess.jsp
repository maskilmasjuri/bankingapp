<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Successful Admin Login</title>
</head>
<body>
<h2>Admin, you have logged in successfully!</h2><br><br>
<form action="/BankingApp/CustomerViewController"><button>View Customers</button></form><br>
<form action="/BankingApp/ViewLoansController"><button>View Loans</button></form><br>
<a href="/BankingApp/UpdateLoanStatus.html"><button>Update Loan Status</button></a><br>

</body>
</html>