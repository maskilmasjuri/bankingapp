<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Successful Registration</title>
</head>
<body>
<h2>${customerName}, you registered successfully!</h2>
<br><br>
<button>Login</button>
</body>
</html>