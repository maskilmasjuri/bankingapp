<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Successful Customer Login</title>
</head>
<body>
<h2>${sName }, you have logged in successfully!</h2><br><br>
<form action="/BankingApp/CheckBalanceController"><button>Check Balance</button></form><br>
<form action="/BankingApp/CustomerResetPasswordController"><button>Change Password</button></form><br>
<form action="/BankingApp/MakePaymentController"><button>Make Payment</button></form><br>
<form action="/BankingApp/ApplyLoanController"><button>Apply for Loan</button></form><br>
<a href="/BankingApp/ViewTransactions.jsp"><button>View Transactions</button></a><br>

</body>
</html>