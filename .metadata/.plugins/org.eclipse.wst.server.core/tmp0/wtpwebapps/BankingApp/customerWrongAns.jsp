<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Security Question</title>
</head>
<body>

	<h2>Wrong answer, try again</h2>
	<form action="/BankingApp/SecurityAnsController" >
		<table>
			<tr>
				<td>${securityQn}</td>
				<td><input type="text" name="securityAns"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Answer"></td>
			</tr>
		</table>
	</form>
</body>
</html>